//
//  LOTRConverter17App.swift
//  LOTRConverter17
//
//  Created by Bjorn Ongenae on 17/05/2024.
//

import SwiftUI

@main
struct LOTRConverter17App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
